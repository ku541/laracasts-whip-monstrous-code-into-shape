<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'TutorialController@store');

// Auth::loginUsingId(1);

// Route::get('/', function () {
//     $user = App\User::find(1);

//     return view('welcome', compact('user'));
// });

Route::post('posts', function () {
    $form = new \App\Http\Requests\PublishPostRequest;

    $form->persist();

    return 'Success!';
});

Route::get('purchases', 'PurchasesController@store');

Route::get('users', 'UsersController@store');

Route::get('teams', 'TeamMembersController@store');
