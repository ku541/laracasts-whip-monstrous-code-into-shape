<?php

namespace App\Validators;

use Illuminate\Validation\ValidationException;

class Korean
{
    /**
     * Ensure that there's no Korean in the body.
     * 
     * @param Request $request
     * @throws ValidationException 
     */
    public function search(Request $request)
    {
        if (preg_match("/[\p{Hangul}]+/u/", $request->title)) {
            throw new ValidationException;
        }

        if (preg_match("/[\p{Hangul}]+/u/", $request->body)) {
            throw new ValidationException;
        }

        if (preg_match("/[ㄱㄴㄷㄹㅁㅂㅅㅇㅈㄲㄸㅃㅆㅉㅊㅋㅌㅍㅎㅏㅐㅑㅒㅓㅔㅕㅖㅗㅘㅙㅚㅛㅜㅝㅞㅟㅠㅡㅢㅣ]/", $request->body)) {
            throw new ValidationException;
        }
    }
}