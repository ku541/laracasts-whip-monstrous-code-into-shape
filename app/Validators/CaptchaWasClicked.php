<?php

namespace App\Validators;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Config\Repository as Config;

use App\Utilities\Curl;

class CaptchaWasClicked
{
    /**
     * @var Curl 
     */
    protected $curl;

    /**
     * @var Config 
     */
    protected $config;

    /**
     * Create a new class instance.
     * 
     * @param Curl $curl
     * @param Config $config 
     */
    public function __construct(Curl $curl, Config $config) {
        $this->curl = $curl;
        $this->config = $config;
    }
    
    /**
     * Assert that the user clicked Google's Recaptcha button.
     *
     * @param  Request $request
     * @throws ValidationException
     */
    public function search(Request $request)
    {
        $response = json_decode(
            $this->curl->post('https://www.google.com/recaptcha/api/siteverify', [
                'secret' => $this->config->get('services.recaptcha.secret'),
                'response' => $request->input('g-recaptcha-response'),
                'remoteip' => $request->ip()
            ])
        );

        if (! $response->success) {
            throw new ValidationException;
        }
    }
}