<?php

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

namespace App\Validators;

class ForbiddenKeywords
{
    /**
     * A list of common spam keywords.
     * 
     * @var array 
     */
    protected $keywords = [
        'megavideo',
        'HD Streaming Online'
    ];

    /**
     * Ensure that there are no forbidden keywords in the body.
     * 
     * @param Request $request
     * @throws ValidationException 
     */
    public function search(Request $request)
    {
        foreach ($this->keywords as $spam) {
            if (stripos($request->body, $spam) !== false) {
                throw new ValidationException;
            }
        }
    }
}