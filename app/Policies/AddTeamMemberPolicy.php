<?php

namespace App\Policies;

use App\Team;

class AddTeamMemberPolicy
{
    protected $team;

    public function __construct(Team $team)
    {
        $this->team = $team;
    }

    public function allows()
    {
        if (auth()->guest()) {
            abort(403, 'You are not signed in.');
        }

        if ($this->team->owner_id != auth()->user()->id) {
            abort(403, 'You are not the owner of this team.');
        }

        if ($this->team->isMaxedOut()) {
            abort(403, 'Your team is maxed out.');
        }
    }
}
