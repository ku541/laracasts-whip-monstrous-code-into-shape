<?php

namespace App\Http\Controllers;

use App\Team;
use App\Policies\AddTeamMemberPolicy;

class TeamMembersController extends Controller
{
    /**
     * Add a new member to the given team
     *
     * @param Team $team
     */
    public function store()
    {
        $team = Team::find(1);

        $this->authorize($team);

        return 'Add the user to the team.';
    }
}
