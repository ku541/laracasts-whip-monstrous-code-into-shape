<?php

namespace App\Http\Controllers;

use App\UseCases\PurchasePodcast;

class PurchasesController extends Controller
{
    public function store()
    {
        dispatch(new PurchasePodcast);

        return 'Done';
    }
}
