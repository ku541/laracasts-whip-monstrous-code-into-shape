<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function store()
    {
        User::register([
            'name' => 'Kusal',
            'email' => 'kusalwettimuny@outlook.com',
            'password' => bcrypt('password')
        ]);
    }
}
