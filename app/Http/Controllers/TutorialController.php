<?php

namespace App\Http\Controllers;

use App\Tutorials\TweetsTutorial;
use App\Tutorials\PublishesTutorial;

class TutorialController extends Controller
{
    public function store()
    {
        $tutorial = new TweetsTutorial(new PublishesTutorial);

        return $tutorial->publish();
    }
}
