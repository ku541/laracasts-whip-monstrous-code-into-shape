<?php

namespace App;

trait Completable
{
    public function completions()
    {
        // Fetch completions relationship.
    }

    public function complete()
    {
        // Reference relationship to complete item.
    }

    public function uncomplete()
    {
        
    }
}
