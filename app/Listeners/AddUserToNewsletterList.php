<?php

namespace App\Listeners;

use App\Events\UserRegistered;

class AddUserToNewsletterList
{
    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        var_dump('Use Campaing Monitor to add ' . $event->user->email . ' to the newsletter list.');
    }
}
