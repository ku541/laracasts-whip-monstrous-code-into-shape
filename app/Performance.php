<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Performance extends Model
{
    public function getRevenueAttribute($revenue)
    {
        return new Revenue($revenue);
    }

    // public function revenueInDollars()
    // {
    //     return $this->revenue() / 100;
    // }
    //
    // public function revenueAsCurrency()
    // {
    //     return money_format('$%i', $this->revenueInDollars());
    // }
}
