<?php

namespace App;

trait ParticipatesInForum
{
    public function startConversation(Conversation $conversation)
    {
        // Create a new thread for the current user.
    }

    public function replyTo(Conversation $conversation, Reply $reply)
    {
        // Have a user reply to a conversation with the given reply.
    }
}
